﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

public static class Encryptor
{
    public static string Encrypt(string clearText)
    {
        byte[] bytes = Encoding.Unicode.GetBytes(clearText);
        PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes("Captcha_Key", new byte[13] {
		Convert.ToByte(73),
		Convert.ToByte(118),
		Convert.ToByte(97),
		Convert.ToByte(110),
		Convert.ToByte(32),
		Convert.ToByte(77),
		Convert.ToByte(101),
		Convert.ToByte(100),
		Convert.ToByte(118),
		Convert.ToByte(101),
		Convert.ToByte(100),
		Convert.ToByte(101),
		Convert.ToByte(118)
	});
        return Convert.ToBase64String(Encryptor.Encrypt(bytes, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16)));
    }

    public static string Decrypt(string cipherText)
    {
        byte[] cipherData = Convert.FromBase64String(cipherText.Replace(" ", "+"));
        PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes("Captcha_Key", new byte[13] {
		Convert.ToByte(73),
		Convert.ToByte(118),
		Convert.ToByte(97),
		Convert.ToByte(110),
		Convert.ToByte(32),
		Convert.ToByte(77),
		Convert.ToByte(101),
		Convert.ToByte(100),
		Convert.ToByte(118),
		Convert.ToByte(101),
		Convert.ToByte(100),
		Convert.ToByte(101),
		Convert.ToByte(118)
	});
        return Encoding.Unicode.GetString(Encryptor.Decrypt(cipherData, passwordDeriveBytes.GetBytes(32), passwordDeriveBytes.GetBytes(16)));
    }

    private static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
    {
        using (MemoryStream memoryStream = new MemoryStream())
        {
            Aes aes__1 = Aes.Create();
            aes__1.Padding = PaddingMode.ISO10126;
            aes__1.Key = Key;
            aes__1.IV = IV;
            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, aes__1.CreateEncryptor(), CryptoStreamMode.Write))
            {
                cryptoStream.Write(clearData, 0, clearData.Length);
                cryptoStream.Close();
            }
            return memoryStream.ToArray();
        }
    }

    private static byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
    {
        using (MemoryStream memoryStream = new MemoryStream())
        {
            Aes aes__1 = Aes.Create();
            aes__1.Padding = PaddingMode.ISO10126;
            aes__1.Key = Key;
            aes__1.IV = IV;
            using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, aes__1.CreateDecryptor(), CryptoStreamMode.Write))
            {
                cryptoStream.Write(cipherData, 0, cipherData.Length);
                cryptoStream.Close();
            }
            return memoryStream.ToArray();
        }
    }

}