﻿using LibCryptoUTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace MVC_AsistenciaDocente
{
    public class QSEncriptedValueProvider : DictionaryValueProvider<String>
    {
        public QSEncriptedValueProvider(ControllerContext controllerContext)
            : base(GetQSDictionary(controllerContext), Thread.CurrentThread.CurrentCulture)
        {
        }


        private static IDictionary<string, string> GetQSDictionary(ControllerContext controllerContext)
        {
            var dict = new Dictionary<string, string>();
            var req = controllerContext.HttpContext.Request;
            foreach (var key in req.QueryString.AllKeys)
            {
                var value = req.QueryString[key];
                EncriptaUTP cryp = new EncriptaUTP();
                try
                {
                    dict.Add(key.Substring(1), cryp.DesencriptaAES128(value, "qwe123asd"));
                }
                catch (Exception)
                {
                }
                
            }
            return dict;
        }
    }
}
