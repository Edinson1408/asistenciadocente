﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVC_AsistenciaDocente
{
    public class QSEncriptedValueProviderFactory : ValueProviderFactory
    {
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            return new QSEncriptedValueProvider(controllerContext);
        }
    }
}
