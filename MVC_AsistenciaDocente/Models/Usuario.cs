﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_AsistenciaDocente.Models
{
    public class Usuario
    {
        [Required]
        [Display(Name = "Usuario")]
        public string usu { get; set; }
        [Required]
        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        public string pwd { get; set; }

        //
        public string local { get; set; }
        public int anio { get; set; }
        public int mes { get; set; }
        public int dia { get; set; }
        public int hora { get; set; }
        public int min { get; set; }

        public DateTime fecha
        {
            get
            {
                if (anio > 0)
                {
                    try
                    {
                        return new DateTime(anio, mes, dia, hora, min, 0);
                    }
                    catch (Exception)
                    {

                        return DateTime.Now;
                    }

                }
                else
                {
                    return DateTime.Now;
                }
            }
        }


    }
}