﻿using LibCryptoUTP;
using MVC_AsistenciaDocente.Models;
using MVC_AsistenciaDocente.ServiceAD;
using LICL_AsistenciaDocente_BL;
using LICL_AsistenciaDocente_EL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Web.Security;
using System.Net;
using System.Web.Services.Protocols;
using System.Diagnostics;

using System.Net.Mail;
using System.Text;
using System.IO;

using System.Text.RegularExpressions;

namespace MVC_AsistenciaDocente.Controllers
{
    [HandleError(View = "_Error")]
    public class AsistenciaController : Controller
    {
      
        #region Objetos Globales
        Exception _ex;
        E_Parametros parametros;
        B_Docente obDocente;
        B_General oGeneral;
        
        B_Terminal obTerminal;

        //AsistenciaController ip = "10.60.8.12";      

        Stopwatch stopWatch;

        #endregion

        public string local_ps
        {
            get
            {
                if (parametros.validarIP == "1" & ConfigurationManager.AppSettings["TST"].ToString() == "0")
                {
                     var local_ps = obTerminal.validarIP_PS(ip, ref _ex);
                     Session["vUnidad"] = obTerminal.validarUnidad_PS(local_ps, ref _ex);
                     ViewBag.local_ps = local_ps;
                     return local_ps; //obTerminal.validarIP_PS( ip, ref _ex);
                }
                else
                {
                    if (Session["vlocal"] != null)
                    {
                        ViewBag.local_ps = Session["vlocal"].ToString();
                        return Session["vlocal"].ToString();
                    }
                    else
                    {
                        ViewBag.local_ps = "**";
                        return "**";
                    }
                }
            }
        }
 
        public string ip
        {
            /* original  */
            /*  get
            {
                //JBRAVO 15-02                
                return Request.ServerVariables["REMOTE_ADDR"].ToString().Replace(":", ".");
            }            
            */
            /* prueba   */
            get
            {
                return "10.20.30.4";
            }
           
        }

        public DateTime Fec_Actual
        {
            get
            {
                if (ConfigurationManager.AppSettings["TST"].ToString() == "0")
                    return DateTime.Now;
                else
                    return (DateTime)Session["vfecha"];
            }
        }

        #region ActionsResults
        [AllowAnonymous]
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }
        #endregion

        [AllowAnonymous]
        public ActionResult Login(string returnURL)
        {
            try
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();
                //if (ConfigurationManager.AppSettings["TST"].ToString() == "0")
                //{

                //    if (parametros.tipo_acceso == "2") // PEOPLESOFT
                //    {
                //        //string ip2;
                //        //ip2='10.60.8.12';
                //        ViewBag.local_ps = obTerminal.validarIP_PS(ip, ref _ex);
                //    }                   
                //}
                //else
                //{
                //    ViewBag.local_ps = "**";
                //}

                if (parametros.validarIP == "1")
                {
                    if (parametros.tipo_acceso == "2")
                    {
                        // Process.Start("shutdown.exe", " -l");      
                        if (local_ps == "")
                        {
                            stopWatch.Stop();
                            var tasktime = stopWatch.ElapsedMilliseconds;

                            obTerminal.registraLog_ps("Anonimo", ip, "", "NAU", Request.Browser.Browser + " " + Request.Browser.Version + " ", tasktime, ref _ex);
                            ViewBag.loginmsg = "No puede acceder al sistema por encontrarse en una sede no válida para la marcación de asistencia.";
                        }
                    }                    
                }
                else
                {
                    ViewBag.local_ps = "**";
                }

                if (Request.IsAuthenticated)
                  return RedirectToAction("Horario");
                //Process.Start("shutdown.exe", " -l");
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Usuario oUsuario)        
        {
            stopWatch = new Stopwatch();
            stopWatch.Start();
            String msg = "";

            if (parametros.tipo_acceso == "2")
            {
                ViewBag.local_ps = obTerminal.validarIP_PS(ip, ref _ex);
            }
            
            if (parametros.validarIP == "1")
            {
                if (parametros.tipo_acceso == "2")
                {
                    if (local_ps.Trim() == "")
                    {
                        stopWatch.Stop();
                        var tasktime = stopWatch.ElapsedMilliseconds;

                        obTerminal.registraLog_ps("Anonimo", ip, "", "NOA", Request.Browser.Browser + " " + Request.Browser.Version + " ", tasktime, ref _ex);
                        ViewBag.loginmsg = "No puede acceder al sistema por encontrarse en una sede no válida para la marcación de asistencia.";
                        return View();
                    }
                }                
            }

            if (ConfigurationManager.AppSettings["TST"].ToString() == "1")
            {
                if (oUsuario.anio == 0)
                    Session["vfecha"] = DateTime.Now;
                else
                    Session["vfecha"] = new DateTime(oUsuario.anio, oUsuario.mes, oUsuario.dia, oUsuario.hora, oUsuario.min, 0);

                Session["vlocal"] = oUsuario.local;
            }

            oUsuario.usu = (oUsuario.usu == null) ? oUsuario.usu : oUsuario.usu.ToUpper();

            //if (parametros.tipo_acceso == "2")
            //{
            //    obTerminal.registraLog_ps(oUsuario.usu, ip, local_ps, "INT", Request.Browser.Browser + " " + Request.Browser.Version + " ", ref _ex);
            //}            

            try
            {
                if ((oUsuario.pwd == "" || oUsuario.usu == "") || (oUsuario.pwd == null || oUsuario.usu == null))
                {
                    msg = "Complete los campos obligatorios";
                    goto NoLogin;
                }
                
                //JBRAVO 
                if (parametros.validarAD == "1")
                {
                    int result = 0;
                    ServiceADSoapClient proxy = new ServiceADSoapClient();
                    EncriptaUTP cryp = new EncriptaUTP();
                    /*result = proxy.validaUsuarioAD(oUsuario.usu.Trim(), cryp.EncriptaAES128(oUsuario.pwd, "-+*%$({[]})$%*+-"), "ASIST");*/
                    result = 1;
                    if (result == 0)
                    {
                        msg = "Usuario y/o contraseña incorrecta";
                        goto NoLogin;
                    }
                }

                FormsAuthentication.SetAuthCookie(oUsuario.usu, true);
                Session["usuario"] = oUsuario.usu;
                Session["docente"] = oUsuario.usu;

                if (parametros.tipo_acceso == "2")
                {
                    stopWatch.Stop();
                    var tasktime = stopWatch.ElapsedMilliseconds;

                    obTerminal.registraLog_ps(oUsuario.usu, ip, local_ps, "ING", Request.Browser.Browser + " " + Request.Browser.Version, tasktime, ref _ex);
                }                

                return RedirectToAction("Horario");
            }
            catch (Exception ex)
            {
                string sError = ex.InnerException.ToString();
                sError = Regex.Replace(sError, @"[^0-9A-Za-z]", " ", RegexOptions.None);
                stopWatch.Stop();
                var tasktime = stopWatch.ElapsedMilliseconds;
                obTerminal.registraLog_ps(oUsuario.usu, ip, local_ps, "NOI", sError, tasktime, ref _ex);
               
                throw;
                //return RedirectToAction("Error", new { msg = ex.InnerException.Message });
            }

        NoLogin:

            if (parametros.tipo_acceso == "2")
            {
                stopWatch.Stop();
                var tasktime = stopWatch.ElapsedMilliseconds;

                obTerminal.registraLog_ps(oUsuario.usu, ip, local_ps, "NOI", Request.Browser.Browser + " " + Request.Browser.Version, tasktime, ref _ex);
            }
        
            ModelState.AddModelError("", msg);            
            return View();
        }

        public ActionResult LogOff()
        {
            try
            {
                //FormsAuthentication.SignOut();
                //return RedirectToAction("Login");
                Session.Clear();
                Session.Abandon();
                FormsAuthentication.SetAuthCookie(null, false);
                FormsAuthentication.SignOut();
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", new { msg = ex.Message });
            }
        }

        public ActionResult Horario(string msg = "")
        {
            //if (!Request.IsAuthenticated)
            //    return RedirectToAction("Login");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            string _msg = "";

            /* adicionado */
            string _local_dep = "";
            string _estado = "";
            bool _flag = false;
            bool _bReprog = false;

            try
            {
                if (Session["usuario"] == null)
                    return RedirectToAction("LogOff");

                string usuario = Session["usuario"].ToString();
                string docente = Session["docente"].ToString();

                List<E_Horario> horario = new List<E_Horario>(); 
                E_Horario oHorarioObs = null;
                _ex = null;
                /*---------------- Redirección a la web de restricción ---------------------*/
                if (parametros.tipo_acceso == "2") // PEOPLESOFT
                {
                    /*---------------- Original ---------------------
                    horario = obDocente.listarHorarioDocente_PS(docente, Fec_Actual, usuario, ref _ex).OrderBy(m => m.hora_inicio).ToList();
                    horario = depurarHorarios_ps(horario, ref _msg, ref oHorarioObs);
                   ---------------- Original ---------------------*/
                    if (usuario.Substring(0, 1).ToUpper() == "C" || usuario.Substring(0, 1).ToUpper() == "E")
                    {
                        horario = obDocente.listarHorarioDocente_PS(docente, Fec_Actual, usuario, ref _ex).OrderBy(m => m.hora_inicio).ToList();                        
                        horario = depurarHorarios_ps(horario, ref _msg, ref _local_dep, ref _estado, ref _flag, ref _bReprog, ref oHorarioObs);
                    }
                    else if (usuario.Substring(0, 1).ToUpper() == "A" || usuario.Substring(0, 1).ToUpper() == "D")
                    {
                        horario = obDocente.listarHorarioDocente_PS(docente, Fec_Actual, usuario, ref _ex).OrderBy(m => m.hora_inicio).ToList();
                        horario = depurarHorarios_ps(horario, ref _msg, ref _local_dep, ref _estado, ref _flag, ref _bReprog, ref oHorarioObs);
                       
                        ViewBag.campus = ConfigurationManager.AppSettings["CAMP"].ToString(); 
                        ViewBag.msjacceso = "PROBANDO";                        
                    }
                }                
                /*---------------- Redirección a la web de restricción ---------------------*/
                // original
                ViewBag.msg = msg;
                ViewBag.msg2 = _msg;

                ViewBag.toleracionInicial = parametros.tol_inicial_idat;
                ViewBag.curso = oHorarioObs;
                ViewBag.ip  = ip;
                //@ViewBag.local_as = local_as;

                @ViewBag.local_ps = _local_dep;                
                ViewBag.confirmacionAsistalu = _estado;
                ViewBag.flag = _flag;
                ViewBag.Reprogramado = _bReprog;

                ViewBag.confirmacionAsistencia = "";
                if (Session["Guardo_Asistencia"] != null)
                {
                    if (Session["Guardo_Asistencia"].ToString() == "OK")
                    {
                        ViewBag.confirmacionAsistencia = "Se registró la asistencia de alumnos y delegado correctamente. Recuerde que puede volver a modificarla mientras dure su clase.";
                        Session["Guardo_Asistencia"] = null;
                    }
                    else if (Session["Guardo_Asistencia"].ToString() == "NOK")
                    {
                        ViewBag.confirmacionAsistencia = "No se pudo registrar la asistencia de alumnos. Por favor intente nuevamente.";
                        Session["Guardo_Asistencia"] = null;
                    }
                    Session["Guardo_Asistencia"] = null;
                }

                if (oHorarioObs != null)
                {
                    stopWatch.Stop();
                    var tasktime = stopWatch.ElapsedMilliseconds;
                    obTerminal.registraLog_ps(oHorarioObs.codDocente, ip, local_ps, "RAD", "Pre registro de asistencia Docente" + "|" + oHorarioObs.CLASS_NBR, tasktime, ref _ex);
                }
                                
                Session["notifyDelegado"] = false;
                Session["DiferenciaIPs"] = (oHorarioObs != null) ? obTerminal.getDiferenciaIPs(oHorarioObs.codDocente, local_ps, oHorarioObs.CLASS_NBR, ref _ex) : false;

                return View(horario.ToList());
            }
            catch (Exception ex)
            {
                string sError = ex.Message.ToString();
                sError = Regex.Replace(sError, @"[^0-9A-Za-z]", " ", RegexOptions.None);
                stopWatch.Stop();
                var tasktime = stopWatch.ElapsedMilliseconds;

                obTerminal.registraLog_ps(Session["docente"].ToString(), ip, local_ps, "RAD", sError.Substring(0, (sError.Length > 90) ? 90 : sError.Length), tasktime, ref _ex);
                return RedirectToAction("Error", new { msg = ex.Message });
            }
        }

        //J.BRAVO 08-03-2017
        [HttpPost]
        public JsonResult Revocardelegado(string sInstitucion, string sGrado, string sStrm, string sClase, string sDelegado)
        {
            obDocente.updFlagDelegado(sInstitucion, sGrado, sStrm, sClase, sDelegado, ref _ex);                               
            return Json(true);
        }

        public ActionResult Asistencia(string _json)
        {
            try
            {
                if (Request.IsAuthenticated)
                {
                    stopWatch = new Stopwatch();
                    stopWatch.Start();

                    LibCryptoUTP.EncriptaUTP cryp = new LibCryptoUTP.EncriptaUTP();
                    var json = cryp.DesencriptaAES128(_json, "qwe123asd");
                    var oeHorario = (E_Horario)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(E_Horario));
                    try
                    {
                        E_Delegado flagDelegado = new E_Delegado();
                        
                        flagDelegado = obDocente.getDelegado(oeHorario.INSTITUTION, oeHorario.ACAD_CAREER, oeHorario.STRM, oeHorario.CLASS_NBR, ref _ex).LastOrDefault();

                        if (flagDelegado != null)
                        {
                            string sDelegado = flagDelegado.DelegadoCodigoUTP;
                            string sEmplid = flagDelegado.DelegadoEmplid;
                            string sName = flagDelegado.DelegadoNombre;

                            bool flagDelegadoSancionado = obDocente.getDelegadoSancionado(oeHorario.INSTITUTION, oeHorario.ACAD_CAREER, oeHorario.STRM, oeHorario.CLASS_NBR, sDelegado, ref _ex);
                            if (flagDelegadoSancionado)
                            {
                                obDocente.updFlagDelegado(oeHorario.INSTITUTION, oeHorario.ACAD_CAREER, oeHorario.STRM, oeHorario.CLASS_NBR, sDelegado, ref _ex);
                                ViewBag.emplid = "";
                                ViewBag.delegado = "";
                                ViewBag.name = "";
                                ViewBag.flagDelegado = "0";
                            }
                            else
                            {
                                ViewBag.emplid = sEmplid;
                                ViewBag.delegado = sDelegado;
                                ViewBag.name = sName;
                                ViewBag.flagDelegado = "1"; 
                            }
                        }
                        else
                        {
                            ViewBag.emplid = "";
                            ViewBag.delegado = "";
                            ViewBag.name = "";
                            ViewBag.flagDelegado = "0"; 
                        }

                        string _flag = obDocente.getFlagReprogramacion_(oeHorario.INSTITUTION, oeHorario.STRM, oeHorario.CLASS_NBR, oeHorario.fecha_programacion.ToString("dd/MM/yyyy"), oeHorario.hora_termino.ToString("HH:mm"), ref _ex);
                        //if (_flag == "R")
                        //{
                        //    ViewBag.msjError = "No puede registrar la asistencia porque se encuentra en una clase reprogramada";
                        //    return View();
                        //}

                        @ViewBag.institucion = oeHorario.INSTITUTION;
                        @ViewBag.grado = oeHorario.ACAD_CAREER;
                        @ViewBag.strm = oeHorario.STRM;
                        @ViewBag.Browser = Request.Browser.Browser;

                        @ViewBag.docente = oeHorario.nomDocente;
                        @ViewBag.curso = oeHorario.nomCurso;
                        @ViewBag.clase = oeHorario.CLASS_NBR;

                        stopWatch.Stop();
                        var tasktime = stopWatch.ElapsedMilliseconds;
                        obTerminal.registraLog_ps(oeHorario.codDocente, ip, local_ps, "RAA", "Pre registro de asistencia de Alumnos" + "|" + oeHorario.CLASS_NBR, tasktime, ref _ex);
                        Session["notifyDelegado"] = (ViewBag.flagDelegado.ToString() == "0") ? true : false;

                        List<E_Alumno> listaAlumnos = obDocente.listarAlumnos(oeHorario, _flag, ref _ex);
                        return View(listaAlumnos);
                    }
                    catch (Exception ex)
                    {
                        return RedirectToAction("Error", new { msg = ex.Message });
                    }
                }
                else
                {
                    return RedirectToAction("Login");
                }
            }
            catch (Exception ex)
            {
                ViewBag.msjError = "Ocurrió un error al procesar la petición. " + ex.Message;
                return View();
            }
        }
        
        [HttpPost]
        public ActionResult Asistencia(IEnumerable<E_Alumno> lAlumno, E_Horario oeHorario, string delegado, string flag, string emplid, string curso, string name)
        {         
            if (Request.IsAuthenticated)
            {
                try
                {
                    stopWatch = new Stopwatch();
                    stopWatch.Start();

                    string usuario = HttpContext.User.Identity.Name;                  

                    var detalles = (from t in lAlumno
                                    select new pxMarcacionAlumnos.MARALUDET_TypeShape()
                                    {
                                        ALUMNO = t.ALUMNO,
                                        HORA_INI = t.HORA_INI.ToString("HH:mm"),
                                        MOTIVO = "",
                                        PRESENTE = t.ASISTENCIA ? "Y" : "N",
                                        SALIO_TEMPRANO = "N",
                                        TARDE = "N"
                                    }).ToArray();

                    var px = new pxMarcacionAlumnos.LVF_MARC_ALU_DET();
                    var req = new pxMarcacionAlumnos.LVF_MARALU_REQ_TypeShape();

                    var _lAlumno = lAlumno.ToList();
                    req.CARRERA_ACADEMICA = _lAlumno[0].CARRERA_ACADEMICA;
                    req.CICLO_LECTIVO = _lAlumno[0].CICLO_LECTIVO;
                    req.FECHA_ASISTENCIA = Fec_Actual.ToString("yyyy-MM-dd");
                    req.INSTITUCION = _lAlumno[0].INSTITUCION;
                    req.N_CLASE = _lAlumno[0].N_CLASE;
                    req.CAMPUS = oeHorario.CAMPUS;
                    req.N_SECCION = oeHorario.N_SECCION;
                    req.NRO_CATALOGO = oeHorario.CATALOG_NBR;
                    req.SESION = oeHorario.SESSION;
                    req.MATERIA = oeHorario.MATERIA;
                    req.HORA_INI = _lAlumno[0].HORA_INI.ToString("HH:mm");
                    req.MARALUDET = detalles;

                    var _json = Newtonsoft.Json.JsonConvert.SerializeObject(req);

                    var result = px.LVF_MAR_ALU(req);

                    if (result.RESULTADO == "Correcto")
                    {
                        stopWatch.Stop();
                        var tasktime = stopWatch.ElapsedMilliseconds;
                        obTerminal.registraLog_ps(Session["docente"].ToString(), ip, local_ps, "RAA", "Se registro la asistencia de Alumnos correctamente" + "|" + _lAlumno[0].N_CLASE, tasktime, ref _ex, true, _lAlumno[0].N_CLASE); 
                            
                        if (flag == "0")
                        {
                            if (!String.IsNullOrEmpty(delegado))
                            {
                                obDocente.addDelegado(_lAlumno[0].INSTITUCION, _lAlumno[0].CARRERA_ACADEMICA, _lAlumno[0].CICLO_LECTIVO, _lAlumno[0].N_CLASE, delegado, emplid, Session["docente"].ToString(), ref _ex);

                                string mensaje1 = "<html> " +
                                               "<body> " +
                                                   "<table cellpadding='0' cellspacing='0' width='100%' style='border:#ccc 1px solid'> " +
                                                   "    <tr> " +
                                                   "        <td> " +
                                                   "            <table cellpadding='0' cellspacing='0' width='100%' style='padding: 10px;'> " +
                                                   "                <tr><td colspan='3' width='100%' align='center'><img id='header' src='cid:idImage1'/></td></tr> " +
                                                   "                <tr><td colspan='3' height='30px'></td></tr> " +
                                                   "                <tr> " +
                                                   "                    <td width='20%'></td> " +
                                                   "                    <td width='60%' style='font: normal 16px Tahoma;'> " +
                                                   "                        Estimado(a) Alumno(a),<br><br>El presente es para informarle que ha sido seleccionado como delegado del curso: <b> " + curso + "</b> <br> " +
                                                   "                        de la clase: <b>" + _lAlumno[0].N_CLASE + "</b> con fecha: <b>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "</b> <br> " +
                                                   "                    </td> " +
                                                   "                    <td width='20%'></td> " +
                                                   "                </tr> " +
                                                   "                <tr><td colspan='3' height='50px'></td></tr> " +
                                                   "                <tr><td colspan='3' width='100%' align='center' ><img id='header' src='cid:idImage2'/></td></tr> " +
                                                   "            </table> " +
                                                   "        </td> " +
                                                   "    </tr> " +
                                                   "</table> " +
                                               "</body> " +
                                           "</html> ";
                               
                                enviaCorreo(_lAlumno[0].INSTITUCION, delegado, "[Designación de Delegado – Clase: " + _lAlumno[0].N_CLASE + " Curso: " + curso + " ]", mensaje1, "Servicios Universitarios", "serviciosuniversitarios@utp.edu.pe", false);

                                string sCorDest = String.Empty;
                                string sFirDest = String.Empty;

                                if(_lAlumno[0].INSTITUCION == "UNUTP") {
                                   sFirDest = "Registros Académicos";
                                   sCorDest = "registrosacademicos@utp.edu.pe";
                                }else{
                                    sFirDest = "Calidad Educativa";
                                    sCorDest = "caleduidat@idat.edu.pe";
                                }

                                string mensaje2 = "<html> " +
                                             "<body> " +
                                                 "<table cellpadding='0' cellspacing='0' width='100%' style='border:#ccc 1px solid'> " +
                                                 "    <tr> " +
                                                 "        <td> " +
                                                 "            <table cellpadding='0' cellspacing='0' width='100%' style='padding: 10px;'> " +
                                                 "                <tr><td colspan='3' width='100%' align='center'><img id='header' src='cid:idImage1'/></td></tr> " +
                                                 "                <tr><td colspan='3' height='30px'></td></tr> " +
                                                 "                <tr> " +
                                                 "                    <td width='20%'></td> " +
                                                 "                    <td width='60%' style='font: normal 16px Tahoma;'> " +
                                                 "                        Estimado(a) Docente,<br><br>El presente es para informarle que ha seleccionado como delegado del curso: <b> " + curso + "</b> <br> " +
                                                 "                        de la clase: <b>" + _lAlumno[0].N_CLASE + "</b> al Alumno: <b>" + name + "</b> con fecha: <b>" + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + "</b> <br> " +
                                                 "                    </td> " +
                                                 "                    <td width='20%'></td> " +
                                                 "                </tr> " +
                                                 "                <tr><td colspan='3' height='50px'></td></tr> " +
                                                 "                <tr><td colspan='3' width='100%' align='center'><img id='header' src='cid:idImage2'/></td></tr> " +
                                                 "            </table> " +
                                                 "        </td> " +
                                                 "    </tr> " +
                                                 "</table> " +
                                             "</body> " +
                                         "</html> ";
                               
                                enviaCorreo(_lAlumno[0].INSTITUCION, Session["docente"].ToString(), "[Designación de Delegado – Clase: " + _lAlumno[0].N_CLASE + " Curso: " + curso + " ]", mensaje2, sFirDest, sCorDest, true);
                            }               
                        }
                        Session["Guardo_Asistencia"] = "OK";
                    }
                    else
                    {
                        stopWatch.Stop();
                        var tasktime = stopWatch.ElapsedMilliseconds;
                        obTerminal.registraLog_ps(Session["docente"].ToString(), ip, local_ps, "RAA", "Se produjo un error al intentar registrar la asistencia de Alumnos" + "|" + _lAlumno[0].N_CLASE, tasktime, ref _ex); 
                        Session["Guardo_Asistencia"] = "NOK";
                    }
                    return RedirectToAction("Horario", "Asistencia");
                }
                catch (SoapException ex)
                {
                    string sError = ex.Detail.InnerText.ToString();
                    sError = Regex.Replace(sError, @"[^0-9A-Za-z]", " ", RegexOptions.None);
                    stopWatch.Stop();
                    var tasktime = stopWatch.ElapsedMilliseconds;

                    obTerminal.registraLog_ps(Session["docente"].ToString(), ip, local_ps, "RAA", sError.Substring(0, (sError.Length > 90) ? 90 : sError.Length) + "|" + oeHorario.CLASS_NBR, tasktime, ref _ex);                           
                    return RedirectToAction("Error", new { msg = ex.Detail.InnerText });
                }
                catch (Exception ex)
                {
                    string sError = ex.Message.ToString();
                    sError = Regex.Replace(sError, @"[^0-9A-Za-z]", " ", RegexOptions.None);
                    stopWatch.Stop();
                    var tasktime = stopWatch.ElapsedMilliseconds;

                    obTerminal.registraLog_ps(Session["docente"].ToString(), ip, local_ps, "RAA", sError.Substring(0, (sError.Length > 90) ? 90 : sError.Length) + "|" + oeHorario.CLASS_NBR, tasktime, ref _ex);                       
                    return RedirectToAction("Error", new { msg = ex.Message });
                }
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public bool enviaCorreo(string empresa, string lcDesCor, string lcDesAsu, string lcDesMen, string lcRemNom, string lcRemCor, bool flag)
        {
            try
            {
                bool mensajesEnProduccion = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["MensajeCorreoEnProduccion"]);
                string usuario = System.Configuration.ConfigurationManager.AppSettings["MensajeCorreoUsuario"];
                string contrasena = System.Configuration.ConfigurationManager.AppSettings["MensajeCorreoContrasena"];
                string host = System.Configuration.ConfigurationManager.AppSettings["MensajeCorreoHost"];

                int puerto = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MensajeCorreoPuerto"]);
                bool enableSSL = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["MensajeCorreoEnableSSL"]);

                MailMessage Message = new MailMessage();

                string correoDe = lcRemCor;
                string correoDeNombre = lcRemNom;

                Message.From = new MailAddress(correoDe, correoDeNombre, Encoding.UTF8);

                //Destinatario correo
                string lcDestinatario = lcDesCor.Trim() + ((empresa == "UNUTP") ? "@utp.edu.pe" : "@idat.edu.pe");

                Message.To.Add(new MailAddress(lcDestinatario, lcDestinatario, Encoding.UTF8));

                //////
                string path = Server.MapPath("~/App_Data/" + ((empresa == "UNUTP") ? "UTP/" : "IDAT/")).ToString();
        
                AlternateView alternateView = AlternateView.CreateAlternateViewFromString(lcDesMen, null, "text/html");
                LinkedResource linkedResource;
                string UrlArchivo = String.Empty;
                UrlArchivo = path + "header.jpg";

                using (WebClient webClient = new WebClient())
                {
                    webClient.UseDefaultCredentials = true;
                    byte[] buf = webClient.DownloadData(UrlArchivo);
                    MemoryStream memoryStream = new MemoryStream(buf);
                    linkedResource = new LinkedResource(memoryStream);
                }

                linkedResource.ContentId = "idImage1";
                alternateView.LinkedResources.Add(linkedResource);

                UrlArchivo = String.Empty;
                //UrlArchivo = System.Configuration.ConfigurationManager.AppSettings["urlAlumnoFoto"];
                UrlArchivo = path + ((flag) ? "firma_docente.jpg" : "firma_alumno.jpg");

                using (WebClient webClient = new WebClient())
                {
                    webClient.UseDefaultCredentials = true;
                    byte[] buf = webClient.DownloadData(UrlArchivo);
                    MemoryStream memoryStream = new MemoryStream(buf);
                    linkedResource = new LinkedResource(memoryStream);
                }

                linkedResource.ContentId = "idImage2";
                alternateView.LinkedResources.Add(linkedResource);
                
                Message.AlternateViews.Add(alternateView);
                //////

                Message.IsBodyHtml = true;

                //Message.Body = lcDesMen;
                Message.Subject = lcDesAsu; //prefijoAsunto + " " + lcDesAsu;
                Message.SubjectEncoding = Encoding.UTF8;

                //if (!flag && empresa == "UNUTP") { Message.Attachments.Add(new Attachment(path + "manual.pdf")); }

                SmtpClient client = new SmtpClient();
                client.Host = host;
                client.Port = puerto;
                client.UseDefaultCredentials = false;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                //27MAR15: Comentado en desarrollo para homolgar con UTP.
                //Notar que en desarrollo el correo NO se envía al estar comentada estas dos líneas.
                client.EnableSsl = enableSSL;
                client.Credentials = new NetworkCredential(usuario, contrasena);  //Cuenta en criteria para el envío de correos.

                client.Send(Message);
                //await Task.Yield();
                return true;
            }
            catch (Exception loError)
            {
                return false;
            }
        }

        #region asistencia

        //public ActionResult asistenciaDocente(E_Horario oeHorario)
        public ActionResult asistenciaDocente(string _json)
        {
            if (Request.IsAuthenticated)
            {
                stopWatch = new Stopwatch();
                stopWatch.Start();

                LibCryptoUTP.EncriptaUTP cryp = new LibCryptoUTP.EncriptaUTP();
                var json = cryp.DesencriptaAES128(_json, "qwe123asd");
                var oeHorario = (E_Horario)Newtonsoft.Json.JsonConvert.DeserializeObject(json, typeof(E_Horario));
                DateTime fecha = Fec_Actual;
                try
                {
                    if (parametros.tipo_acceso == "2")
                    {
                        if (obDocente.marcarAsistencia_PS(oeHorario, fecha, ref _ex) != "0")
                        {
                            stopWatch.Stop();
                            var tasktime = stopWatch.ElapsedMilliseconds;
                            obTerminal.registraLog_ps(oeHorario.codDocente, ip, local_ps, "RAD", "Se registro la asistencia Docente correctamente" + "|" + oeHorario.CLASS_NBR, tasktime, ref _ex, true, oeHorario.CLASS_NBR); 
                            return RedirectToAction("Horario");
                        }
                    }
                    stopWatch.Stop();
                    var tasktime1 = stopWatch.ElapsedMilliseconds;
                    obTerminal.registraLog_ps(oeHorario.codDocente, ip, local_ps, "RAD", "Se produjo un error al intentar registrar la asistencia Docente" + "|" + oeHorario.CLASS_NBR, tasktime1, ref _ex);     
                    return RedirectToAction("Horario");
                }
                catch (Exception ex)
                {
                    string sError = ex.Message.ToString();
                    sError = Regex.Replace(sError, @"[^0-9A-Za-z]", " ", RegexOptions.None);
                    stopWatch.Stop();
                    var tasktime = stopWatch.ElapsedMilliseconds;

                    obTerminal.registraLog_ps(oeHorario.codDocente, ip, local_ps, "RAD", sError.Substring(0, (sError.Length > 90) ? 90 : sError.Length) + "|" + oeHorario.CLASS_NBR, tasktime, ref _ex);                           
                    return RedirectToAction("Error", new { msg = ex.Message });
                }
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult asistenciaAlumno()
        {

            try
            {
                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", new { msg = ex.Message });
            }
        }

        public ActionResult Error(string msg)
        {
            return RedirectToAction("LogOff");
        }
        #endregion

        private List<E_Horario> depurarHorarios_ps(List<E_Horario> L_Horarios, ref string _msg, ref string _local_dep, ref string _estado, ref bool _flag, ref bool _bReprog, ref E_Horario oHorarioObs)
        {
            string estasist = "";

            if (L_Horarios.Count() == 0)
            {
                return L_Horarios;
            }

            E_Horario cursor = null;
            int tEntrada = parametros.tol_inicial_idat;
            int tSalida = parametros.tol_final_idat;
            DateTime fecIni;
            DateTime fecFin;
          
            int minFalta = 0;

            L_Horarios = L_Horarios.OrderByDescending(m => m.hora_inicio).ToList();
            string local_dep = "";
            foreach (E_Horario item in L_Horarios)
            {
                fecIni = new DateTime(Fec_Actual.Year, Fec_Actual.Month, Fec_Actual.Day, item.hora_inicio.Hour, item.hora_inicio.Minute, 0);
                fecFin = new DateTime(Fec_Actual.Year, Fec_Actual.Month, Fec_Actual.Day, item.hora_termino.Hour, item.hora_termino.Minute, 0);

                item.observacion = item.marcacion_entrada.Hour > 0 ? item.marcacion_entrada.ToShortTimeString() : "No Registra";

                minFalta = (int)(fecIni - Fec_Actual).TotalMinutes;
                
                if (DateTime.Compare(Fec_Actual, fecFin.AddMinutes(tSalida).AddMinutes(-1)) <= 0)
                {
                    _flag = (item.observacion.ToString() == "No Registra") ? false : true;
                    item.observacion = item.marcacion_entrada.Hour > 0 ? item.marcacion_entrada.ToShortTimeString() : "";

                    cursor = item;
                    item.minFaltantes = minFalta;
                    cursor.minFaltantes = minFalta;
                    //Dentro de la hora en el local
                    if (DateTime.Compare(Fec_Actual, fecIni.AddMinutes(tEntrada * -1)) >= 0 & DateTime.Compare(Fec_Actual, fecFin) <= 0)
                    {
                        item.enclase = true;

                        /* agregado */

                        string _estasistalu = obDocente.getFlagReprogramacion_(item.INSTITUTION, item.STRM, item.CLASS_NBR, item.fecha_programacion.ToString("dd/MM/yyyy"), item.hora_termino.ToString("HH:mm"), ref _ex);
                        if (_estasistalu == "C" || _estasistalu == "R")
                        {                            
                            estasist = "";
                            _bReprog = (_estasistalu == "R") ? true : false;
                        }
                        else
                        {
                            estasist = "Todavía no ha realizado la toma de asistencia de alumno";
                        }
                        /* fin */

                        /* ORIGINAL 

                        if (local_ps == cursor.codLocal || local_ps == "**" || cursor.tipoPrograma == "E")
                            item.enLocal = true;
                        */

                        List<E_Terminales> listlocal_ps = new List<E_Terminales>();
                        //E_Terminales listlocalps = null;
                        _ex = null;

                        listlocal_ps = obTerminal.ListaLocalxIP_PS(ip, ref _ex);
                        //listlocal_ps = depurarLocalesxip_ps(listlocal_ps, ref listlocalps);
                        // item.enLocal = false;
                        foreach (E_Terminales items in listlocal_ps)
                        {
                            if (items.idLocal == cursor.codLocal || items.idLocal == "**" || cursor.tipoPrograma == "E")

                            local_dep = items.idLocal;
                            item.enLocal = true;
                        }
                    }
                }
            }

            //Enclases
            if (L_Horarios.Where(m => m.enclase == true).Count() > 0)
            {
                //En otro local
                /*original
                if (cursor.codLocal != local_ps & local_ps != "**" & cursor.tipoPrograma != "E")
                */
                //if (cursor.codLocal != local_ps & local_ps != "**" & cursor.codLocal != "10110105" & cursor.tipoPrograma != "E")

                /* CAMBIO HE - 020517 */

                if (cursor.codLocal != local_dep & local_dep != "**" & cursor.tipoPrograma != "E")
                {
                    _msg = "01";

                    /*adicion */
                    _local_dep = local_dep;

                    oHorarioObs = cursor;
                }

                /* adición */
                else
                {
                    _estado = estasist;

                    /*adicion */
                    _local_dep = local_dep;

                    oHorarioObs = cursor;


                }
                /* adición fin */

                /*en desarrollo*/
                _msg = "";
                _estado = estasist;
                _local_dep = local_dep;
                oHorarioObs = cursor;

            }
            //En clase proximas
            else if (L_Horarios.Where(m => m.minFaltantes > 0).Count() > 0)
            {
                /* original
                //En Local
                if (cursor.codLocal == local_ps & local_ps != "**" & cursor.tipoPrograma != "E")
                {
                    _msg = "02";
                    oHorarioObs = cursor;
                }
                */

                /* cambio */
                if (cursor.codLocal == local_dep & local_dep != "**" & cursor.tipoPrograma != "E")
                {
                    _msg = "02";
                    /*adicion */
                    _local_dep = local_dep;
                    oHorarioObs = cursor;
                }
                else
                {
                    _msg = "03";
                    /*adicion */
                    _local_dep = local_dep;
                    oHorarioObs = cursor;
                }
            }
            else
            {
                _msg = "04";
                /*adicion */
                _local_dep = local_dep;
            }

            return L_Horarios.OrderBy(m => m.hora_inicio).ToList();
        }

        #region constructor
        public AsistenciaController()
        {
            _ex = null;

            //parametros = obTerminal.qryParametrosSistema(ref _ex);
            parametros = new E_Parametros();
            parametros.validarIP = ConfigurationManager.AppSettings["VIP"].ToString();
            parametros.validarAD = ConfigurationManager.AppSettings["VAD"].ToString();
            parametros.tipo_acceso = ConfigurationManager.AppSettings["TIP"].ToString();

            parametros.tol_inicial_idat = int.Parse(ConfigurationManager.AppSettings["TII"].ToString());
            parametros.tol_inicial_utp = int.Parse(ConfigurationManager.AppSettings["TIU"].ToString());
            parametros.tol_inicial_epg = int.Parse(ConfigurationManager.AppSettings["TIE"].ToString());
            parametros.tol_final_idat = int.Parse(ConfigurationManager.AppSettings["TFI"].ToString());
            parametros.tol_final_utp = int.Parse(ConfigurationManager.AppSettings["TFU"].ToString());
            parametros.tol_final_epg = int.Parse(ConfigurationManager.AppSettings["TFE"].ToString());

            //string caddb2 = ConfigurationManager.ConnectionStrings["AS400"].ConnectionString.ToString();
            string cadora = ConfigurationManager.ConnectionStrings["ORACLE"].ConnectionString.ToString();
            /* ORIGINAL
            oGeneral = new B_General(caddb2, cadora); */

            /*CAMBIO HE */
            oGeneral = new B_General(cadora);

            obDocente = new B_Docente();
            obTerminal = new B_Terminal();
        }
        #endregion
    }
}