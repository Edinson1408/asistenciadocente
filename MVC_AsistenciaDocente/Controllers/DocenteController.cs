﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LICL_AsistenciaDocente_BL;
using LICL_AsistenciaDocente_EL;

namespace MVC_AsistenciaDocente.Controllers
{
    public class DocenteController : Controller
    {
        B_Docente obDocente;
        E_Horario horario;
        public ActionResult Index()
        {
            E_Docente oeDocente = (E_Docente)Session["docente"];
            horario = (E_Horario)Session["horario"];
            ViewBag.Nombres = oeDocente.nombre;
            return View(horario);
        }



        public ActionResult marcarIngreso()
        {
            horario = (E_Horario)Session["horario"];
            return View();
        }

        public ActionResult marcarSalida()
        {
            horario = (E_Horario)Session["horario"];
            return View();
        }

        public ActionResult tomarAsistencia()
        {
            horario = (E_Horario)Session["horario"];
            //CALL GUSLIB.gue2_GU6002R1('02','C00145',2015,3,1,'A','62Z',1,05,0945,1115)
            //string empresa, string docente, string año, string periodo, string num_dia, string grupo_curso, string codigo_curso, string turno, string seccion, string hora_inicio, string hora_fin
            List<E_Alumno> listaAlumnos = obDocente.asistenciaAlumnos(horario.codigo_empresa, horario.codigo_docente, horario.anio_academico, horario.periodo_academico, horario.dia_semana, horario.grupo_curso, horario.codigo_curso, horario.turno, horario.seccion, horario.hora_inicio, horario.hora_termino);
            return View(listaAlumnos);
        }

        public DocenteController()
        {
            obDocente = new B_Docente();
        }
    }
}
