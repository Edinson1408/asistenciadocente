﻿using LICL_AsistenciaDocente_DL;
using LICL_AsistenciaDocente_EL;
using System;
using System.Collections.Generic;


namespace LICL_AsistenciaDocente_BL
{
    public class B_Terminal
    {
        D_Terminal oTerminal;

       //original
        public string validarIP_PS(string ip, ref Exception _ex)
        {
            return oTerminal.qryLocalxIP_PS(ip, ref _ex);
        }

        //original
        public string validarUnidad_PS(string ip, ref Exception _ex)
        {
            return oTerminal.qryUnidadxIP_PS(ip, ref _ex);
        }

        /* cambio x HE - 0205  */
        //#region IP
        public List<E_Terminales> ListaLocalxIP_PS(string ip, ref Exception _ex)
        {
            return oTerminal.ListaLocalxIP_PS (ip, ref _ex);
        }

        public Boolean getDiferenciaIPs(string doc, string local, string nclase, ref Exception _ex)
        {
            return oTerminal.getDiferenciaIPs(doc, local, nclase, ref _ex);
        }

        public void registraLog_ps(string doc, string ip, string local, string accion, string obs, decimal tasktime, ref Exception _ex, Boolean op = false, string nclase = null)
        {
            oTerminal.registraLog_ps(doc, ip, local, accion, obs, tasktime, op, nclase, ref _ex);            
        }

        public B_Terminal()
        {
            oTerminal = new D_Terminal();
        }

    }
}
