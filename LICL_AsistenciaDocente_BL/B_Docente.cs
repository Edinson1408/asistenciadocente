﻿using LICL_AsistenciaDocente_DL;
using LICL_AsistenciaDocente_EL;
using System;
using System.Collections.Generic;


using lvf_docente = LICL_AsistenciaDocente_BL.LVF_ASIS_DOC;
using lvf_alumno = LICL_AsistenciaDocente_BL.LVF_ASIS_ALU;

namespace LICL_AsistenciaDocente_BL
{
    public class B_Docente
    {
        D_Docente odDocente;

        #region Docente
        public List<E_Horario> listarHorarioDocente_PS(string cod_Docente, DateTime fecha, string usuario, ref Exception _ex)
        {
            return odDocente.listarHorarioDocente_PS(cod_Docente, fecha, usuario, ref _ex);
        }

        public string marcarAsistencia_PS(E_Horario oeHorario, DateTime fecha, ref Exception _ex)
        {
            lvf_docente.Marcacion_Docente client = new lvf_docente.Marcacion_Docente();
            lvf_docente.LVF_MAR_DOC_REQ_TypeShape req = new lvf_docente.LVF_MAR_DOC_REQ_TypeShape();
            lvf_docente.LVF_MAR_DOC_RES_TypeShape res = new lvf_docente.LVF_MAR_DOC_RES_TypeShape();
            req.INSTITUCION = oeHorario.INSTITUTION;
            req.COD_EMPLEADO = oeHorario.emplID;
            req.FECHA = oeHorario.fecha_programacion.ToString("yyyy/MM/dd");
            req.HORA_INICIO = oeHorario.hora_inicio.ToString("HH:mm");
            req.HORA_FIN = oeHorario.hora_termino.ToString("HH:mm");
            req.CICLO_LECTIVO = oeHorario.STRM;
            req.JUSTIFICACION = "A";
            req.N_CLASE = oeHorario.CLASS_NBR;
            req.CAMPUS = oeHorario.CAMPUS;
            req.HORA_ENTRADA = fecha.ToString("HH:mm");
            req.HORA_SALIDA = oeHorario.hora_termino.ToString("HH:mm");
            res = client.LVF_MARC_DOCENTE(req);
            return res.RESULTADO;
        }
            
        public string getFlagReprogramacion(string empresa, string periodo, string clase, string fecha, string hora, ref Exception _ex)
        {
            return odDocente.getFlagReprogramacion(empresa, periodo, clase, fecha, hora, ref _ex);
        }

        public string getFlagReprogramacion_(string empresa, string periodo, string clase, string fecha, string hora, ref Exception _ex)
        {
            return odDocente.getFlagReprogramacion_(empresa, periodo, clase, fecha, hora, ref _ex);
        }
        #endregion

        //J.BRAVO 08-03-2017
        public void addDelegado(string empresa, string grado, string periodo, string clase, string alumno, string emplid, string user, ref Exception _ex)
        {
            odDocente.addDelegado(empresa, grado, periodo, clase, alumno, emplid, user, ref _ex);
        }

        public void updFlagDelegado(string empresa, string grado, string periodo, string clase, string alumno, ref Exception _ex)
        {
            odDocente.updFlagDelegado(empresa, grado, periodo, clase, alumno, ref _ex);
        }

        public List<E_Delegado> getDelegado(string empresa, string grado, string periodo, string clase, ref Exception _ex)
        {
            return odDocente.getDelegado(empresa, grado, periodo, clase, ref _ex);
        }

        public bool getDelegadoSancionado(string empresa, string grado, string periodo, string clase, string alumno, ref Exception _ex)
        {
            return odDocente.getDelegadoSancionado(empresa, grado, periodo, clase, alumno, ref _ex);
        }
        /////////////////

        #region Alumno
        public List<E_Alumno> listarAlumnos(E_Horario oeHorario, string _flag, ref Exception _ex)
        {
            return odDocente.listarAlumnos(oeHorario, _flag, ref _ex);
        }

        public void marcarAsistenciaAlumno(E_Horario oeHorario, E_Alumno oAlumno, DateTime fecha, string usuario, ref Exception _ex)
        {
            lvf_alumno.LVF_MAR_ALU_REQ_TypeShape req = new lvf_alumno.LVF_MAR_ALU_REQ_TypeShape();
            lvf_alumno.LVF_MAR_ALU_RES_TypeShape res = new lvf_alumno.LVF_MAR_ALU_RES_TypeShape();
            lvf_alumno.Marcacion_Alumnos client = new lvf_alumno.Marcacion_Alumnos();
            req.INSTITUCION = oAlumno.INSTITUCION;
            req.CICLO_LECTIVO = oAlumno.CICLO_LECTIVO;
            req.MATERIA = oeHorario.MATERIA;
            req.N_CATALOGO = oeHorario.CATALOG_NBR;
            req.CARRERA_ACADEMICA = oAlumno.CARRERA_ACADEMICA;
            req.CAMPUS = oeHorario.CAMPUS;
            req.SESION = oeHorario.SESSION;
            req.N_CLASE = oAlumno.N_CLASE;
            req.N_SECCION = oeHorario.N_SECCION;
            req.FECHA_ASISTENCIA = fecha.ToString("yyyy-MM-dd"); ;
            req.ALUMNO = oAlumno.ALUMNO;
            req.PRESENTE = oAlumno.ASISTENCIA ? "Y" : "N";
            req.TARDE = "N";
            req.SALIO_TEMPRANO = "N";
            req.MOTIVO = "";
            //req.HORA_INI = oAlumno.ASISTENCIA ? fecha.ToString("HH:MM") : "";
            req.HORA_INI = oAlumno.HORA_INI.ToString("HH:mm");
            res = client.LVF_MARC_ALUMNO(req);
        }
        #endregion

        public B_Docente()
        {
            odDocente = new D_Docente();
        }
    }
}
