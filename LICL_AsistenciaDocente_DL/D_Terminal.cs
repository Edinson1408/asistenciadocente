﻿using LICL_AsistenciaDocente_EL;
using System;
using System.Collections.Generic;
using System.Data;

using LICL_Oracle_Helper;

namespace LICL_AsistenciaDocente_DL
{
    public class D_Terminal : D_Data
    {
        OracleHelper oHelper;

        //ORIGINAL
        public string qryLocalxIP_PS(string ip, ref Exception _ex)
        {
            try
            {
                // original
                string sql = "SELECT LOCATION FROM SYSADM.PS_UTP_IP_RANG_TBL WHERE '{0}' LIKE TRIM(IP_RANG) AND TRIM(IP_RANG) <>  '%.%.%.%'";                                
                sql = string.Format(sql, ip);
                DataTable tbl = oHelper.query_sql(sql, ref _ex);
                if (tbl.Rows.Count > 0) return tbl.Rows[0][0].ToString().Trim();
            }
            catch (Exception ex)
            {
                ex = _ex;
            }
            return "";
        }

        //ORIGINAL
        public string qryUnidadxIP_PS(string ip, ref Exception _ex)
        {
            try
            {
                // original
                string sql = " SELECT DISTINCT A.LOCATION, B.INSTITUTION " +
                             "     FROM SYSADM.PS_FACILITY_TBL A " +
                             "     INNER JOIN SYSADM.PS_LVF_INST_UNEG B ON B.SETID=A.SETID AND B.FACILITY_ID=A.FACILITY_ID    " +
                             "     AND B.EFFDT =  " +
                             "     (  " +
                             "       SELECT MAX(EFFDT) FROM SYSADM.PS_FACILITY_TBL T  " +
                             "       WHERE T.SETID=A.SETID AND T.FACILITY_ID=A.FACILITY_ID  " +
                             "       AND T.EFFDT <= TO_DATE(TO_CHAR(SYSDATE,'YYYY-MM-DD'),'YYYY-MM-DD')  " +
                             "     )  " +
                            // "     WHERE LOCATION='{0}' AND ROWNUM <=1  " +
                             "     WHERE LOCATION='{0}'  " +
                             "     ORDER BY 2 DESC ";
                sql = string.Format(sql, ip);
                DataTable tbl = oHelper.query_sql(sql, ref _ex);
                return (tbl.Rows.Count > 0) ? tbl.Rows[0][1].ToString().Trim() : "UNUTP";
            }
            catch (Exception ex)
            {
                ex = _ex;
            }
            return "";
        }        

        /* CAMBIADO el 02/05/17 - HE  */
        public List<E_Terminales> ListaLocalxIP_PS(string ip, ref Exception _ex)
        {
            List<E_Terminales> listaLocalxIP_PS = new List<E_Terminales>();
            string sql = "SELECT LOCATION FROM SYSADM.PS_UTP_IP_RANG_TBL WHERE '{0}' LIKE TRIM(IP_RANG)";
            sql = string.Format(sql, ip);

            using (DataTable tbl = oHelper.query_sql(sql, ref _ex))
            {
                E_Terminales oTerminal;

                foreach (DataRow fila in tbl.Rows)
                {
                    oTerminal = new E_Terminales();

                    oTerminal.idLocal = fila["LOCATION"].ToString().Trim();
                    //oTerminal.idEmpresa = fila["NUMBER_LVF1"].ToString().Trim();
                    //oTerminal.strIPIni = fila["IP_RANG"].ToString().Trim();
                    listaLocalxIP_PS.Add(oTerminal);
                }
            }

            return listaLocalxIP_PS;
        }

        public Boolean getDiferenciaIPs(string doc, string local, string nclase, ref Exception _ex)
        {
            DateTime hoy = DateTime.Now;
            string nfecha = hoy.ToString("yyyyMMdd");
            try
            {
                string sql = " SELECT DISTINCT IPAMBI FROM SYSADM.UTP_LOG_ASISDOC NOLOCK " +
                             "   WHERE USRNAM='{0}' " +
                             "   AND CODLOC='{1}' " +
                             "   AND TO_CHAR(FECREG,'YYYYMMDD') = '{2}' " +
                             "   AND (CASE WHEN INSTR(OBSERV, '|') > 0 THEN SUBSTR(OBSERV, INSTR(OBSERV, '|')+1 ,10) ELSE ' ' END) = '{3}' " +
                             "   AND OBSERV LIKE '%Se registro%' ";
                sql = string.Format(sql, doc, local, nfecha, nclase);
                DataTable tbl = oHelper.query_sql(sql, ref _ex);
                return (tbl.Rows.Count > 1) ? true : false;
            }
            catch (Exception ex)
            {
                ex = _ex;
            }
            return false;
        }
         
        public void registraLog_ps(string doc, string ip, string local, string accion, string obs, decimal time, Boolean op, string nclase, ref Exception _ex)
        {
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("dd/MM/yyyy HH:mm:ss");
            string nfecha = hoy.ToString("yyyyMMdd");

            oHelper.exec_Command("INSERT INTO SYSADM.UTP_LOG_ASISDOC(FECSRV, USRNAM, IPAMBI, CODLOC, ACCION, OBSERV, TIEMPO) VALUES(TO_DATE('" + fecha + "','DD-MM-YYYY HH24:MI:SS'),'" + doc + "','" + ip + "','" + local + "','" + accion + "','" + obs + "'," + time + ")", ref _ex);
        
            if (op)
            {
                string sql = " SELECT DISTINCT IPAMBI FROM SYSADM.UTP_LOG_ASISDOC NOLOCK " +
                             "   WHERE USRNAM='{0}' " +
                             "   AND CODLOC='{1}' " +
                             "   AND TO_CHAR(FECREG,'YYYYMMDD') = '{2}' " +
                             "   AND (CASE WHEN INSTR(OBSERV, '|') > 0 THEN SUBSTR(OBSERV, INSTR(OBSERV, '|')+1 ,10) ELSE ' ' END) = '{3}' " +
                             "   AND OBSERV LIKE '%Se registro%' ";
                sql = string.Format(sql, doc, local, nfecha, nclase);
                DataTable tbl = oHelper.query_sql(sql, ref _ex);

                if (tbl.Rows.Count > 1)
                {
                    string sql2 = " SELECT COUNT(DISTINCT IPAMBI) AS CT FROM SYSADM.UTP_LOG_ASISDOC NOLOCK " +
                                    "   WHERE USRNAM='{0}' " +
                                    "   AND CODLOC='{1}' " +
                                    "   AND TO_CHAR(FECREG,'YYYYMMDD') = '{2}' " +
                                    "   AND (CASE WHEN INSTR(OBSERV, '|') > 0 THEN SUBSTR(OBSERV, INSTR(OBSERV, '|')+1 ,10) ELSE ' ' END) = '{3}' " +
                                    "   AND ACCION = 'DIF' ";
                    sql2 = string.Format(sql2, doc, local, nfecha, nclase);
                    DataTable tbl2 = oHelper.query_sql(sql2, ref _ex);
                    if (tbl2.Rows[0][0].ToString() == "0")
                    {
                        oHelper.exec_Command("INSERT INTO SYSADM.UTP_LOG_ASISDOC(FECSRV, USRNAM, IPAMBI, CODLOC, ACCION, OBSERV, TIEMPO) VALUES(TO_DATE('" + fecha + "','DD-MM-YYYY HH24:MI:SS'),'" + doc + "','" + ip + "','" + local + "','DIF','" + "Diferencia de IPs en registro|" + nclase + "'," + time + ")", ref _ex);
                    }
                }
            }

        }

        public D_Terminal()
        {
            oHelper = new OracleHelper(cnOracle);
        }
    }
}
