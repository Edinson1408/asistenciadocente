﻿using LICL_AsistenciaDocente_EL;
using LICL_Oracle_Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;


namespace LICL_AsistenciaDocente_DL
{
    public class D_Docente : D_Data
    {
        OracleHelper orcHelper;
        #region Docente

        public List<E_Horario> listarHorarioDocente_PS(string _codDocente, DateTime _fecha, string _usuario, ref Exception _ex)
        {

            string sEmplid = String.Empty;
            //string sql1 = "SELECT EMPLID FROM SYSADM.PS_LVF_COD_DOCE_VW WHERE LVF_COD_EMPL='{0}'";
            string sql1 = "SELECT * FROM SYSADM.PS_LVF_EMPL_AS400 WHERE '{0}' IN (UPPER( TRIM(LVF_COD_EMPL)),UPPER(TRIM(LVF_COD_EMPL1)), UPPER( TRIM(LVF_COD_EMPL2)), UPPER(TRIM(LVF_COD_AD))) ORDER BY EFFDT DESC";

            sql1 = string.Format(sql1, _codDocente.Trim().ToUpper());

            using (DataTable tbl1 = orcHelper.query_sql(sql1, ref _ex))
            {
                sEmplid = tbl1.Rows[0][0].ToString();
            }

            List<E_Horario> lstHorario = new List<E_Horario>();

            //string sql = "SELECT * FROM SYSADM.PS_LVF_CLASDOC_VW1 WHERE '{0}' IN (UPPER( TRIM(LVF_COD_EMPL)),UPPER(TRIM(LVF_COD_EMPL1)), UPPER( TRIM(LVF_COD_EMPL2)), UPPER(TRIM(LVF_COD_AD))) AND TO_CHAR(MEETING_DT,'YYYYMMDD')='{1}'";
            string sql = "SELECT * FROM SYSADM.PS_UTP_CLAS_DOCVW1 A WHERE EMPLID = '" + sEmplid + "' AND TO_CHAR(MEETING_DT,'YYYYMMDD') = '{1}'";
           
            sql = string.Format(sql, _codDocente.Trim().ToUpper(), _fecha.ToString("yyyyMMdd"));

            using (DataTable tbl = orcHelper.query_sql(sql, ref _ex))
            {

                lstHorario = tbl.AsEnumerable().Select(dataRow => new E_Horario
                {
                    tipo =  "PS",
                    codDocente = _codDocente,
                    INSTITUTION = dataRow.Field<String>("INSTITUTION"),
                    emplID = dataRow.Field<String>("EMPLID"),
                    ACAD_CAREER = dataRow.Field<String>("ACAD_CAREER"),
                    fecha_programacion = dataRow.Field<DateTime>("MEETING_DT"),
                    hora_inicio = dataRow.Field<DateTime>("MEETING_TIME_START"),
                    hora_termino = dataRow.Field<DateTime>("MEETING_TIME_END"),
                    STRM = dataRow.Field<String>("STRM"),
                    CLASS_NBR = (dataRow.Field<Decimal>("CLASS_NBR")).ToString(),
                    diaSemana = (dataRow.Field<Decimal>("LVF_POS_DAY")).ToString(),
                    codAmbiente = dataRow.Field<String>("ROOM"),
                    codCurso = dataRow.Field<String>("CRSE_ID"),
                    nomCurso = dataRow.Field<String>("COURSE_TITLE_LONG"),
                    CAMPUS = dataRow.Field<String>("CAMPUS"),
                    codLocal = dataRow.Field<String>("LOCATION"),
                    dirAmbiente = dataRow.Field<String>("DESCR3"),
                    FIRST_NAME = dataRow.Field<String>("FIRST_NAME"),
                    LAST_NAME = dataRow.Field<String>("LAST_NAME"),
                    SECOND_LAST_NAME = dataRow.Field<String>("SECOND_LAST_NAME"),
                    MATERIA = dataRow.Field<String>("SUBJECT"),
                    SESSION = dataRow.Field<String>("SESSION_CODE"),
                    N_SECCION = dataRow.Field<String>("CLASS_SECTION"),
                    CATALOG_NBR = dataRow.Field<String>("CATALOG_NBR"),

                    marcacion_entrada = (dataRow.Field<DateTime?>("LVFP15_HOR_ENTF") == null) ? new DateTime() : dataRow.Field<DateTime>("LVFP15_HOR_ENTF"),
                    marcacion_salida = (dataRow.Field<DateTime?>("LVFP15_HOR_SALF") == null) ? new DateTime() : dataRow.Field<DateTime>("LVFP15_HOR_SALF"),

                }).ToList();

            }

            return lstHorario; 
        }

        public string getFlagReprogramacion(string empresa, string periodo, string clase, string fecha, string hora, ref Exception _ex)
        {
            string _return = "";
            string sql = " select DISTINCT A.LVF_ESTADO_ASIST  " +
                            " from SYSADM.PS_LVF_EST_L_ASIST A " +
                            " INNER JOIN SYSADM.PS_CLASS_ATTENDNCE B ON A.INSTITUTION=B.INSTITUTION AND A.STRM=B.STRM AND A.CLASS_NBR=B.CLASS_NBR AND A.ATTEND_TMPLT_NBR=B.ATTEND_TMPLT_NBR " +
                            " WHERE A.INSTITUTION='" + empresa + "' AND A.STRM='" + periodo + "' AND A.CLASS_NBR='" + clase + "' AND TO_CHAR(B.CLASS_ATTEND_DT,'DD/MM/YYYY')='" + fecha + "' AND  TO_CHAR(B.ATTEND_FROM_TIME,'HH24:MI')='" + hora + "'";

            using (DataTable tbl = orcHelper.query_sql(sql, ref _ex))
            {
                foreach (DataRow fila in tbl.Rows)
                {
                    _return = Convert.ToString(fila["LVF_ESTADO_ASIST"]);
                }
            }
            return _return;
        }

        public string getFlagReprogramacion_(string empresa, string periodo, string clase, string fecha, string hora, ref Exception _ex)
        {
            string _return = "";
            string sql = " select DISTINCT A.LVF_ESTADO_ASIST  " +
                            " from SYSADM.PS_LVF_EST_L_ASIST A " +
                            " INNER JOIN SYSADM.PS_CLASS_ATTENDNCE B ON A.INSTITUTION=B.INSTITUTION AND A.STRM=B.STRM AND A.CLASS_NBR=B.CLASS_NBR AND A.ATTEND_TMPLT_NBR=B.ATTEND_TMPLT_NBR " +
                            " WHERE A.INSTITUTION='" + empresa + "' AND A.STRM='" + periodo + "' AND A.CLASS_NBR='" + clase + "' AND TO_CHAR(B.CLASS_ATTEND_DT,'DD/MM/YYYY')='" + fecha + "' AND  TO_CHAR(B.ATTEND_TO_TIME,'HH24:MI')='" + hora + "'";

            using (DataTable tbl = orcHelper.query_sql(sql, ref _ex))
            {
                foreach (DataRow fila in tbl.Rows)
                {
                    _return = Convert.ToString(fila["LVF_ESTADO_ASIST"]).ToString().Trim();
                }
            }
            return _return;
        }
       
        //J.BRAVO 08-03-2018
        public void addDelegado(string empresa, string grado, string periodo, string clase, string alumno, string emplid, string user, ref Exception _ex)
        {
            DateTime hoy = DateTime.Now;
            string fecha = hoy.ToString("dd/MM/yyyy HH:mm:ss");
            orcHelper.exec_Command("INSERT INTO SYSADM.PS_UTP_DELEGA_TBL(INSTITUTION, ACAD_CAREER, STRM, CLASS_NBR, LVF_COD_UTP, EMPLID, OPRID, DTTM_STAMP, PRIMARY_NID) VALUES('" + empresa + "', '" + grado + "', '" + periodo + "', '" + clase + "', '" + alumno + "', '" + emplid + "', '" + user + "', TO_DATE('" + fecha + "','DD-MM-YYYY HH24:MI:SS'), 'Y')", ref _ex);
        }

        public void updFlagDelegado(string empresa, string grado, string periodo, string clase, string alumno, ref Exception _ex)
        {
            orcHelper.exec_Command("UPDATE SYSADM.PS_UTP_DELEGA_TBL SET PRIMARY_NID='N' WHERE INSTITUTION ='" + empresa + "' AND ACAD_CAREER='" + grado + "' AND STRM='" + periodo + "' AND CLASS_NBR='" + clase + "'  AND LVF_COD_UTP='" + alumno + "'  ", ref _ex);
        }

        public List<E_Delegado> getDelegado(string empresa, string grado, string periodo, string clase, ref Exception _ex)
        {
            List<E_Delegado> lstDelegado = new List<E_Delegado>();

            string sql = "SELECT DISTINCT A.LVF_COD_UTP, A.EMPLID, B.NAME FROM SYSADM.PS_UTP_DELEGA_TBL A " +
                         "INNER JOIN SYSADM.PS_PERSONAL_DATA B ON A.EMPLID = B.EMPLID " +
                         "WHERE INSTITUTION='" + empresa + "' AND ACAD_CAREER='" + grado + "' AND STRM='" + periodo + "' AND CLASS_NBR='" + clase + "' AND PRIMARY_NID='Y' ";

            using (DataTable tbl = orcHelper.query_sql(sql, ref _ex))
            {
                lstDelegado = tbl.AsEnumerable().Select(dataRow => new E_Delegado
                {
                    DelegadoCodigoUTP = dataRow.Field<String>("LVF_COD_UTP"),
                    DelegadoEmplid = dataRow.Field<String>("EMPLID"),
                    DelegadoNombre = dataRow.Field<String>("NAME")
                }).ToList();
            }

            return lstDelegado;
        }

        public bool getDelegadoSancionado(string empresa, string grado, string periodo, string clase, string alumno, ref Exception _ex)
        {
            bool _return = false;
            string sql = "SELECT COUNT(1) AS FLG FROM PS_STDNT_ENRL A " +
                         "INNER JOIN PS_CLASS_TBL B ON A.STRM = B.STRM AND A.SESSION_CODE = B.SESSION_CODE AND A.CLASS_NBR = B.CLASS_NBR AND A.STDNT_ENRL_STATUS = 'D' " +
                         "LEFT JOIN SYSADM.PS_ENRL_RSN_TBL C ON C.SETID='GLOBL' AND C.ACAD_CAREER = A.ACAD_CAREER AND C.ENRL_ACTION = A.STDNT_ENRL_STATUS AND C.ENRL_ACTION_REASON = A.ENRL_ACTN_RSN_LAST " +
                         "INNER JOIN SYSADM.PS_LVF_COD_ALU_TBL D ON D.EMPLID=A.EMPLID AND D.INSTITUTION=A.INSTITUTION " +
                         "WHERE A.INSTITUTION='" + empresa + "' AND A.ACAD_CAREER='" + grado + "' AND A.STRM='" + periodo + "' AND A.CLASS_NBR='" + clase + "'  AND D.LVF_COD_UTP='" + alumno + "'  " +
                         "GROUP BY A.INSTITUTION, A.EMPLID, D.LVF_COD_UTP, A.ACAD_CAREER, A.STRM, A.CLASS_NBR, C.DESCR "; 

            using (DataTable tbl = orcHelper.query_sql(sql, ref _ex))
            {
                foreach (DataRow fila in tbl.Rows)
                {
                    _return = (Convert.ToString(fila["FLG"]) == "1") ? true : false;
                }
            }
            return _return;
        }

        public List<E_Alumno> listarAlumnos(E_Horario horario, string _flag, ref Exception _ex)
        {
            List<E_Alumno> listaAlumnos = new List<E_Alumno>();

            string sql = "SELECT A.*, B.LVF_COD_UTP, C.STDNT_ENRL_STATUS, " +
                          "  ( " +
                          "    CASE WHEN C.STDNT_ENRL_STATUS = 'D' THEN " +
                          "    'N'  " +
                          "    ELSE " +
                          "    ATTEND_PRESENT " +
                          "    END  " +
                          "  ) ATTEND_PRESENT_1 " +
                         "FROM SYSADM.PS_LVF_CLAS_ALU_VW A " +
                         "INNER JOIN SYSADM.PS_LVF_COD_ALU_TBL B ON A.EMPLID=B.EMPLID AND A.INSTITUTION=B.INSTITUTION " +
                         "INNER JOIN SYSADM.PS_STDNT_ENRL C ON C.INSTITUTION=A.INSTITUTION AND C.ACAD_CAREER=A.ACAD_CAREER AND C.STRM=A.STRM AND C.CLASS_NBR=A.CLASS_NBR AND C.EMPLID=A.EMPLID " +
                         "WHERE A.INSTITUTION = '{0}' AND A.ACAD_CAREER='{1}' AND A.STRM='{2}' AND A.CLASS_NBR={3} AND TO_CHAR(A.CLASS_ATTEND_DT,'YYYYMMDD')='{4}' AND TO_CHAR(A.ATTEND_TO_TIME,'HH24MI')='{5}' ORDER BY A.LAST_NAME, A.SECOND_LAST_NAME, A.FIRST_NAME ";
            sql = string.Format(sql, horario.INSTITUTION, horario.ACAD_CAREER, horario.STRM, horario.CLASS_NBR, horario.fecha_programacion.ToString("yyyyMMdd"), horario.hora_termino.ToString("HHmm"));

            using (DataTable tbl = orcHelper.query_sql(sql, ref _ex))
            {

                listaAlumnos = tbl.AsEnumerable().Select(dataRow => new E_Alumno
                {
                    INSTITUCION = dataRow.Field<String>("INSTITUTION"),
                    CICLO_LECTIVO = dataRow.Field<String>("STRM"),
                    CARRERA_ACADEMICA = dataRow.Field<String>("ACAD_CAREER"),
                    N_CLASE = (dataRow.Field<Decimal>("CLASS_NBR")).ToString(),
                    ALUMNO = dataRow.Field<String>("EMPLID"),
                    PRESENTE = dataRow.Field<String>("ATTEND_PRESENT"),
                    ASISTENCIA = (dataRow.Field<String>("ATTEND_PRESENT_1")).ToString().Trim().ToUpper() == ((dataRow.Field<String>("STDNT_ENRL_STATUS").ToString().Trim().ToUpper() == "D") ? "Y" : ((dataRow.Field<String>("INSTITUTION") == "UNIDT" && _flag == "") ? ((dataRow.Field<String>("ATTEND_PRESENT").ToString().Trim().ToUpper() == "Y") ? "N" : "Y") : "Y")),
                    NOMBRES = dataRow.Field<String>("FIRST_NAME"),
                    APELLIDO_PATERNO = dataRow.Field<String>("LAST_NAME"),
                    APELLIDO_MATERNO = dataRow.Field<String>("SECOND_LAST_NAME"),
                    CODIGO_UTP = dataRow.Field<String>("LVF_COD_UTP"),                   
                    HORA_INI = (dataRow.Field<DateTime?>("ATTEND_TO_TIME") == null) ? new DateTime() : dataRow.Field<DateTime>("ATTEND_TO_TIME"),
                    STATUS = dataRow.Field<String>("STDNT_ENRL_STATUS")
                }).ToList();
                
            }

            return listaAlumnos;
        }

        #endregion

        public D_Docente()
        {
            orcHelper = new OracleHelper(cnOracle);
        }

    }
}
