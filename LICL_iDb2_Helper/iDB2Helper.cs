﻿using IBM.Data.DB2.iSeries;
using System;
using System.Data;

namespace LICL_iDb2_Helper
{
    public class iDB2Helper
    {
        private iDB2Connection cn;
        private iDB2DataAdapter da;
        private iDB2Command cmd;

        //public DataTable query_DataTable(string sp, ref Exception _ex)
        //{
        //    DataTable tbl = null;
        //    try
        //    {
        //        tbl = new DataTable();
        //        da = new iDB2DataAdapter(sp, cn);
        //        da.Fill(tbl);
        //        if (da.SelectCommand.Connection.State == ConnectionState.Open)
        //            da.SelectCommand.Connection.Close();
        //    }
        //    catch (IBM.Data.DB2.iSeries.iDB2DCFunctionErrorException ex)
        //    {
        //        _ex = ex;
        //    }
        //    catch (Exception ex)
        //    {
        //        _ex = ex;
        //    }
        //    return tbl;
        //}


        //public static void tst_Connection(ref Exception _ex)
        //{
        //    try
        //    {
        //        iDB2Connection cn = new iDB2Connection(cadcn);
        //        cn.Open();
        //    }
        //    catch (Exception ex)
        //    {
        //        _ex = ex;
        //    }
        //}


        public iDB2Command armarCMD(string sp, ref Exception _ex, params object[] _parametros)
        {
            iDB2Command cmd = null;

            try
            {
                cmd = new iDB2Command(sp, cn);
                cmd.CommandType = CommandType.StoredProcedure;

                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                iDB2CommandBuilder.DeriveParameters(cmd);
                cn.Close();

                int i = 0;
                foreach (iDB2Parameter parametro in cmd.Parameters)
                {
                    parametro.Value = _parametros[i];
                    i++;
                }

            }
            catch (Exception ex)
            {
                _ex = ex;
            }

            return cmd;
        }

        public DataTable query_StoreProcedure(string sp, ref Exception _ex, params object[] param)
        {
            DataTable tbl = new DataTable();
            try
            {
                tbl = new DataTable();
                iDB2Command cmd = armarCMD(sp, ref _ex, param);
                if (_ex != null) throw _ex;
                da = new iDB2DataAdapter(cmd);
                da.Fill(tbl);
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            return tbl;
        }


        public DataTable query_sql(string sql, ref Exception _ex)
        {
            DataTable tbl = new DataTable();
            try
            {
                tbl = new DataTable();
                iDB2DataAdapter da = new iDB2DataAdapter(sql, cn);
                da.Fill(tbl);
            }
            catch (iDB2CommErrorException ex)
            {
                _ex = ex;
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            return tbl;
        }

        public int exec_StoreProcedure(string sp, ref Exception _ex, params object[] param)
        {
            int result = 0;
            try
            {
                iDB2Command cmd = armarCMD(sp, ref _ex, param);
                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                result = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            finally
            {
                cn.Close();
            }
            return result;
        }

        public string exec_Scalar(string sql)
        {
            string resultado = "";
            try
            {
                cmd = new iDB2Command(sql, cn);
                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                resultado = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cn.Close();
            }
            return resultado;
        }

        public int exec_Command(string sp, ref Exception _ex)
        {
            int result = 0;
            try
            {
                cmd = new iDB2Command(sp, cn);
                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                result = cmd.ExecuteNonQuery();
            }
            catch (IBM.Data.DB2.iSeries.iDB2DCFunctionErrorException ex)
            {
                _ex = ex;
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            finally
            {
                if (cn.State == ConnectionState.Open) cn.Close();
            }
            return 0;
        }

        public iDB2Helper(string cadcn)
        {
            cn = new iDB2Connection(cadcn);
        }
    }
}
