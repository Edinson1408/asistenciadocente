﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.OracleClient;


namespace LICL_Oracle_Helper
{
    public class OracleHelper
    {
        private OracleConnection cn;
        private OracleDataAdapter da;
        private OracleCommand cmd;

        public string tstConnection()
        {
            string res = "";
            try
            {
                res = "OK";
                cn.Open();
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }

            return res;
        }

        public OracleCommand armarCMD(string sp, ref Exception _ex, params object[] _parametros)
        {
            OracleCommand cmd = null;

            try
            {
                cmd = new OracleCommand(sp, cn);
                cmd.CommandType = CommandType.StoredProcedure;

                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                OracleCommandBuilder.DeriveParameters(cmd);
                cn.Close();

                int i = 0;
                foreach (OracleParameter parametro in cmd.Parameters)
                {
                    parametro.Value = _parametros[i];
                    i++;
                }

            }
            catch (Exception ex)
            {
                _ex = ex;
            }

            return cmd;
        }

        public DataTable query_StoreProcedure(string sp, ref Exception _ex, params object[] param)
        {
            DataTable tbl = new DataTable();
            try
            {
                tbl = new DataTable();
                OracleCommand cmd = armarCMD(sp, ref _ex, param);
                if (_ex != null) throw _ex;
                da = new OracleDataAdapter(cmd);
                da.Fill(tbl);
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            return tbl;
        }


        public DataTable query_sql(string sql, ref Exception _ex)
        {
            DataTable tbl = new DataTable();
            try
            {
                tbl = new DataTable();
                OracleDataAdapter da = new OracleDataAdapter(sql, cn);
                da.Fill(tbl);
            }
            catch (OracleException ex)
            {
                _ex = ex;
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            return tbl;
        }



        public int exec_StoreProcedure(string sp, ref Exception _ex, params object[] param)
        {
            int result = 0;
            try
            {
                OracleCommand cmd = armarCMD(sp, ref _ex, param);
                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                result = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            finally
            {
                cn.Close();
            }
            return result;
        }

        public string exec_Scalar(string sql)
        {
            string resultado = "";
            try
            {
                cmd = new OracleCommand(sql, cn);
                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                resultado = cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                cn.Close();
            }
            return resultado;
        }

        public int exec_Command(string sp, ref Exception _ex)
        {
            int result = 0;
            try
            {
                
                cmd = new OracleCommand(sp, cn);
                
                if (cn.State == ConnectionState.Open) cn.Close();
                cn.Open();
                OracleTransaction trans = cn.BeginTransaction();
                cmd.Transaction = trans;
                result = cmd.ExecuteNonQuery();                
                trans.Commit();
            }
            catch (OracleException ex)
            {
                _ex = ex;
            }
            catch (Exception ex)
            {
                _ex = ex;
            }
            finally
            {
                if (cn.State == ConnectionState.Open) cn.Close();
            }
            return 0;
        }

        public OracleHelper(string cadcn)
        {
            cn = new OracleConnection(cadcn);
        }
    }
}
