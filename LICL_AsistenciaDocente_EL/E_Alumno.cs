﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LICL_AsistenciaDocente_EL
{
    public class E_Alumno
    {
        public string INSTITUCION { get; set; }
        public string CICLO_LECTIVO { get; set; }
        public string CARRERA_ACADEMICA { get; set; }
        public string N_CLASE { get; set; }
        public string ALUMNO { get; set; }
        public string PRESENTE { get; set; }
        public DateTime HORA_INI { get; set; }
        public string NOMBRES { get; set; }
        public string APELLIDO_PATERNO { get; set; }
        public string CODIGO_UTP { get; set; }
        public bool ASISTENCIA { get; set; }
        public string APELLIDO_MATERNO { get; set; }
        public string CURSO { get; set; }
        public string STATUS { get; set; }
    }
}

