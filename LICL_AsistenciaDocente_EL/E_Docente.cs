﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LICL_AsistenciaDocente_EL
{
    public class E_Docente
    {
        public string codigo { get; set; }
        public string nombre { get; set; }
        public string apellido_paterno { get; set; }
        public string apellido_materno { get; set; }

        /* agregado el 02/05/17 - HE*/
        public string campus { get; set; }
        public string msjacceso { get; set; }
    }
}
