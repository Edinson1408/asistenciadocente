﻿using System;

namespace LICL_AsistenciaDocente_EL
{
    public class E_Horario
    {
        DateTime fechaActual;

        public string tipo{ get; set; }

        public string codEmpresa { get; set; }
        public string codDocente { get; set; }
        public string grupoCurso { get; set; }
        public string codCurso { get; set; }
        public string nomCurso { get; set; }
        public string codAmbiente { get; set; }
        //public string codlocal { get { return codAmbiente.Substring(0, 2); } }
        public string codLocal { get; set; }
        public string dirAmbiente { get; set; }
        public string anioAcademico { get; set; }
        public string periodoAcademico { get; set; }
        public string diaSemana { get; set; }
        public string turno { get; set; }
        public string seccion { get; set; }
        public string nomDocente { get; set; }
        public string apePatDocente { get; set; }
        public string apeMatDocente { get; set; }
        public int minFaltantes { get; set; }
        //public int fecha_programacion { get; set; }
        //public int hora_inicio { get; set; }
        //public int hora_termino { get; set; }
        public string tipoPrograma { get; set; }
        public bool enLocal { get; set; }
        public bool enclase { get; set; }
        public string observacion { get; set; }



        /*
        public int marca_fecha_entrada { get; set; }
        public int marca_hora_entrada { get; set; }
        public int marca_fecha_salida { get; set; }
        public int marca_hora_salida { get; set; }
        */


        //People
        public string INSTITUTION { get; set; }
        public string ACAD_CAREER { get; set; }
        public string emplID { get; set; }
        public string MATERIA { get; set; }
        public string SESSION { get; set; }
        public string N_SECCION { get; set; }
        public string CATALOG_NBR { get; set; }
        public DateTime fecha_programacion { get; set; }
        public DateTime hora_inicio { get; set; }
        public DateTime hora_termino { get; set; }
        public string STRM { get; set; }
        public string CLASS_NBR { get; set; }
        public string CAMPUS { get; set; }
        public DateTime marcacion_entrada { get; set; }
        public DateTime marcacion_salida { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string SECOND_LAST_NAME { get; set; }

        public E_Horario()
        {
            enLocal = false;
            observacion = "";
        }
    }

}


//using System;

//namespace LICL_AsistenciaDocente_EL
//{
//    public class E_Horario
//    {
//        DateTime fechaActual;

//        public string codEmpresa { get; set; }
//        public string codDocente { get; set; }
//        public string grupoCurso { get; set; }
//        public string codCurso { get; set; }
//        public string nomCurso { get; set; }
//        public string codAmbiente { get; set; }
//        public string anioAcademico { get; set; }
//        public string periodoAcademico { get; set; }
//        public string diaSemana { get; set; }
//        public string turno { get; set; }
//        public string seccion { get; set; }
//        public string nomDocente { get; set; }
//        public string apePatDocente { get; set; }
//        public string apeMatDocente { get; set; }

//        #region Logica de inicio y termino de horario
//        public int hora_inicio { get; set; }
//        public int hora_termino { get; set; }

//        public DateTime fecha_incio_clases
//        {
//            get
//            {
//                return DateTime.Parse(fechaActual.ToString("dd/MM/yyyy") + " " + hora_inicio.ToString("00:00") + ":00");
//            }

//        }
//        public DateTime fecha_termino_clases
//        {
//            get
//            {
//                return DateTime.Parse(fechaActual.ToString("dd/MM/yyyy") + " " + hora_termino.ToString("00:00") + ":00");
//            }
//        }
//        #endregion


//        #region Logica de entrada y salida
//        public int marca_fecha_entrada { get; set; }
//        public int marca_hora_entrada { get; set; }
//        public int marca_fecha_salida { get; set; }
//        public int marca_hora_salida { get; set; }

//        public DateTime marcaEntrada
//        {
//            get
//            {
//                if (marca_fecha_entrada != 0)
//                    return DateTime.Parse(marca_fecha_entrada.ToString("0000/00/00") + " " + marca_hora_entrada.ToString("00:00") + ":00");
//                else
//                    return new DateTime();
//            }
//        }

//        public DateTime marcaSalida
//        {
//            get
//            {
//                if (marca_fecha_salida != 0)
//                    return DateTime.Parse(marca_fecha_salida.ToString("0000/00/00") + " " + marca_hora_salida.ToString("00:00") + ":00");
//                else
//                    return new DateTime();
//            }
//        }
//        #endregion


//        #region mostarBotones
//        public bool boolEntrada
//        {
//            get
//            {
//                return (marca_hora_entrada <= 0);
//            }
//        }

//        public bool boolSalida
//        {
//            get
//            {
//                return (marca_hora_salida <= 0) & !boolEntrada;
//            }
//        }

//        public bool boolAsistencia
//        {
//            get
//            {
//                return true;
//            }
//        }
//        #endregion

//        #region Constructores
//        public E_Horario()
//        {
//            fechaActual = DateTime.Now;
//        }

//        public E_Horario(DateTime _fecha)
//        {
//            fechaActual = _fecha;
//        }
//        #endregion
//    }
//}

