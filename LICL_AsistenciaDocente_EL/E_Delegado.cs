﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LICL_AsistenciaDocente_EL
{
    public class E_Delegado
    {
        public string DelegadoEmplid { get; set; }
        public string DelegadoNombre { get; set; }
        public string DelegadoCodigoUTP { get; set; }
    }
}

