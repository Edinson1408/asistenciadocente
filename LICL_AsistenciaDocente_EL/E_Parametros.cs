﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LICL_AsistenciaDocente_EL
{
    public class E_Parametros
    {
        
        public string validarIP { get; set; }
        public string validarAD { get; set; }
        
        public string tipo_acceso { get; set; }
        
        public int tol_inicial_idat { get; set; }
        public int tol_final_idat { get; set; }


        public int tol_inicial_utp { get; set; }
        public int tol_final_utp { get; set; }

        public int tol_inicial_epg { get; set; }
        public int tol_final_epg { get; set; }
    }
}
