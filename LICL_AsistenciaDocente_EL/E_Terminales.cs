﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LICL_AsistenciaDocente_EL
{
    public class E_Terminales
    {
        public string idEmpresa { get; set; }
        public string idLocal { get; set; }
        public int nroGrupo { get; set; }
        public string strIPIni { get; set; }
        public string strIPFin { get; set; }
    }
}
